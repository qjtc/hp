package com.part;

import sun.misc.Unsafe;

import java.lang.reflect.Field;

public class UnsafeUtil {

    public static Unsafe getUnsafe(){
        Field theUnsafe = null;
        Unsafe unsafe = null;
        try {
            theUnsafe = Unsafe.class.getDeclaredField("theUnsafe");
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        theUnsafe.setAccessible(true);
        try {
            unsafe = (Unsafe) theUnsafe.get(null);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return unsafe;
    }

    public UnsafeUtil() {

    }

    public static void main(String[] args) throws Exception {
        Unsafe unsafe = UnsafeUtil.getUnsafe();
        UnsafeUtil unsafeSample = new UnsafeUtil();

        Field field = UnsafeUtil.class.getDeclaredField("normalInt");
        long objectFieldOffset = unsafe.objectFieldOffset(field);
        System.out.println(unsafe.getInt(unsafeSample, objectFieldOffset));

        Field sField = UnsafeUtil.class.getDeclaredField("staticInt");
        Object base = unsafe.staticFieldBase(sField);
        long staticFieldOffset = unsafe.staticFieldOffset(sField);
        System.out.println(unsafe.getInt(base, staticFieldOffset));

    }
}