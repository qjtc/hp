package com.part;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

public class ToListCollector<T> implements Collector<T,List<T>,List<T>> {
    Function<T,T> function;

    public ToListCollector(Function<T,T> function) {
        this.function = function;
    }

    //供应源--建立新的结果容器
    @Override
    public Supplier<List<T>> supplier() {
        return ArrayList::new;
    }

    //累加器--将元素添加到结果容器中
    @Override
    public BiConsumer<List<T>, T> accumulator() {
        return (List<T> list , T t)-> {
            t = function.apply(t);
            System.out.println(t);
            list.add(t);
        };
    }

    //组合器--合并两个结果容器
    @Override
    public BinaryOperator<List<T>> combiner() {
        return ((list, list2) -> {list.addAll(list2);return list;});
    }

    //恒等函数--对结果容器应用转换
    @Override
    public Function<List<T>, List<T>> finisher() {
//        return t->{
//            System.out.println(t);
//            return t;
//        };
        return Function.identity();
    }

    //返回不可变Characteristics,定义收集器的行为，尤其是是否进行并行归约
    @Override
    public Set<Characteristics> characteristics() {
        return Collections.unmodifiableSet(EnumSet.of(Characteristics.IDENTITY_FINISH , Characteristics.CONCURRENT));
    }
}
