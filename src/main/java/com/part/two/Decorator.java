package com.part.two;

public class Decorator implements WordsHandle {
    protected WordsHandle wordsHandle;

    public Decorator(WordsHandle wordsHandle) {
        this.wordsHandle = wordsHandle;
    }

    public String todo(String message) {
        return wordsHandle.todo(message);
    }
}
