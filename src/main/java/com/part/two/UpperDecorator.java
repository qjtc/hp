package com.part.two;

public class UpperDecorator extends Decorator {
    public UpperDecorator(WordsHandle wordsHandle) {
        super(wordsHandle);
    }

    @Override
    public String todo(String message) {
        return message.toUpperCase();
    }
}
