package com.part.two;

import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class WordsMain {
    public void printMain(){
        System.out.println("请输入一段文字:");
        String message = inputString();
        printMenu();
        String input = inputString();
        if(StringUtils.isBlank(input)){
            return;
        }else {
            String[] choose = input.split(",");
            String[] firstStr = new String[]{choose[0]};
            if(firstStr[0].equals("6")){
                System.out.println(choose(choose, message));
            }else {
                System.out.println(choose(firstStr, message));
            }
        }
    }

    private String inputString(){
        InputStreamReader is = new InputStreamReader(System.in); //new构造InputStreamReader对象
        BufferedReader br = new BufferedReader(is); //拿构造的方法传到BufferedReader中
        try {
            return br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private String choose(String[] choose,String message){
        WordsHandle wordsHandle = new CHandle();
        for (String m : choose) {
            if("1".equals(m)){
                wordsHandle = new EncryptDecorator(wordsHandle);
            }
            if("2".equals(m)){
                wordsHandle = new ReversDecorator(wordsHandle);
            }
            if("3".equals(m)){
                wordsHandle = new UpperDecorator(wordsHandle);
            }
            if("4".equals(m)){
                wordsHandle = new LetterDecorator(wordsHandle);
            }
            if("5".equals(m)){
                wordsHandle = new CutDecorator(wordsHandle);
            }
        }
        return wordsHandle.todo(message);
    }

    private void printMenu(){
        System.out.println("1：加密");
        System.out.println("2：反转字符串");
        System.out.println("3：转成大写");
        System.out.println("4：转成小写");
        System.out.println("5：扩展或者剪裁到10个字符，不足部分用！补充");
        System.out.println("6:用户输入 任意组合，比如 1，3 表示先执行1的逻辑，再执行3的逻辑");
    }
}
