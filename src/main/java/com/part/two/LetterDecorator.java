package com.part.two;

public class LetterDecorator extends Decorator {
    public LetterDecorator(WordsHandle wordsHandle) {
        super(wordsHandle);
    }

    @Override
    public String todo(String message) {
        return message.toLowerCase();
    }
}
