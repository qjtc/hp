package com.part.two;

public class CutDecorator extends Decorator {
    public CutDecorator(WordsHandle wordsHandle) {
        super(wordsHandle);
    }

    @Override
    public String todo(String message) {
        if(message.length()<10){
            String m = "!!!!!!!!!!";
            message = message.substring(0,message.length())+m.substring(message.length(),10);
        }else {
            message = message.substring(0,10);
        }
        return message;
    }
}
