package com.part.two;

import java.io.*;
import java.util.Arrays;

public class IntAndByte {
    //    big endian
    public byte[] intToByteForBig(int num){
        byte[] bytes = new byte[4];
        bytes[0] = (byte)(num & 0xff);
        bytes[1] = (byte)(num >> 8 & 0xff);
        bytes[2] = (byte)(num >> 16 & 0xff);
        bytes[3] = (byte)(num >> 24 & 0xff);
        return bytes;
    }

    public int byteToIntForBig(byte[] bytes) {
        if(bytes.length!=4){
            return 0;
        }
        return ((bytes[0] & 0xff) << 0)
                | ((bytes[1] & 0xff) << 8)
                | ((bytes[2] & 0xff) << 16)
                | ((bytes[3] & 0xff) << 24);
    }

    //    little endian
    public byte[] intToByteForLittle(int num){
        byte[] bytes = new byte[4];
        bytes[3] = (byte)(num & 0xff);
        bytes[2] = (byte)(num >> 8 & 0xff);
        bytes[1] = (byte)(num >> 16 & 0xff);
        bytes[0] = (byte)(num >> 24 & 0xff);
        return bytes;
    }

    public int byteToIntForLittle(byte[] bytes) {
        if(bytes.length!=4){
            return 0;
        }
        return ((bytes[3] & 0xff) << 0)
                | ((bytes[2] & 0xff) << 8)
                | ((bytes[1] & 0xff) << 16)
                | ((bytes[0] & 0xff) << 24);
    }

    public void output(byte[] bytes){
        File outputFile = new File("D:\\output.txt");
        FileOutputStream outputFileStream = null;
        try {
            outputFileStream = new FileOutputStream(outputFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            outputFileStream.write(bytes);
            outputFileStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeFile(String path,String[] strings) throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream(new File(path));
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream,"utf-8");
        BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);
        Arrays.stream(strings).forEach((s -> {
            try {
                bufferedWriter.write(s + "\t\n");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }));
        bufferedWriter.close();
        outputStreamWriter.close();
        fileOutputStream.close();
    }
}
