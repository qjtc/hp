package com.part.two;

import org.apache.commons.lang3.ArrayUtils;

public class ReversDecorator extends Decorator {
    public ReversDecorator(WordsHandle wordsHandle) {
        super(wordsHandle);
    }

    @Override
    public String todo(String message) {
        char[] chars = message.toCharArray();
        ArrayUtils.reverse(chars);
        return new String(chars);
    }
}
