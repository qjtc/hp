package com.part.two;

public class EncryptDecorator extends Decorator {
    public EncryptDecorator(WordsHandle wordsHandle) {
        super(wordsHandle);
    }

    @Override
    public String todo(String message) {
        char[] charArray = message.toCharArray();
        for(int i=0;i<charArray.length;i++){
            charArray[i] = (char)(charArray[i]^222);
        }
        return new String(charArray);
    }
}
