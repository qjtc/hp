package com.part.five;

import org.apache.commons.lang3.time.StopWatch;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

/**
 * Created by Administrator on 2018/2/3.
 */
public class MyCounterMain {
    private final static int threadNum = 10;
    private final static int eachNum = 1000000;

    public static void normalTest() throws InterruptedException {
        StopWatch stopWatch = StopWatch.createStarted();
        MyCounter myCounter = new MyCounter();
        CountDownLatch countDownLatch = new CountDownLatch(threadNum);
        IntStream.range(0,threadNum).forEach(i->new Thread(()-> {
            IntStream.range(0, eachNum).forEach(j -> myCounter.normalIncr());
            countDownLatch.countDown();
        }).start());
        countDownLatch.await();
        System.out.println("count:"+myCounter.getNormalValue()+" normalTest:"+stopWatch.getTime(TimeUnit.MICROSECONDS));
    }

    public static void atomicTest() throws InterruptedException {
        StopWatch stopWatch = StopWatch.createStarted();
        MyCounter myCounter = new MyCounter();
        CountDownLatch countDownLatch = new CountDownLatch(threadNum);
        IntStream.range(0,threadNum).forEach(i->new Thread(()-> {
            IntStream.range(0, eachNum).forEach(j -> myCounter.atomicIncr());
            countDownLatch.countDown();
        }).start());
        countDownLatch.await();
        System.out.println("count:"+myCounter.getAtomicLong()+" atomicTest:"+stopWatch.getTime(TimeUnit.MICROSECONDS));
    }

    public static void volatileTest() throws InterruptedException {
        StopWatch stopWatch = StopWatch.createStarted();
        MyCounter myCounter = new MyCounter();
        CountDownLatch countDownLatch = new CountDownLatch(threadNum);
        IntStream.range(0,threadNum).forEach(i->new Thread(()-> {
            IntStream.range(0, eachNum).forEach(j -> myCounter.volatileIncr());
            countDownLatch.countDown();
        }).start());
        countDownLatch.await();
        System.out.println("count:"+myCounter.getVolatileValue()+" volatileTest:"+stopWatch.getTime(TimeUnit.MICROSECONDS));
    }

    public static void longAdderTest() throws InterruptedException {
        StopWatch stopWatch = StopWatch.createStarted();
        MyCounter myCounter = new MyCounter();
        CountDownLatch countDownLatch = new CountDownLatch(threadNum);
        IntStream.range(0,threadNum).forEach(i->new Thread(()-> {
            IntStream.range(0, eachNum).forEach(j -> myCounter.longAdderIncr());
            countDownLatch.countDown();
        }).start());
        countDownLatch.await();
        System.out.println("count:"+myCounter.getLongAdder()+" longAdderTest:"+stopWatch.getTime(TimeUnit.MICROSECONDS));
    }
}
