package com.part.five;

import com.part.UnsafeUtil;
import sun.misc.Unsafe;

import java.lang.reflect.Field;

/**
 * Created by Administrator on 2018/2/1.
 */
public class UnsafeSample {
    public static int staticInt = 123456;
    public int normalInt = 123454;

    public static void main(String[] args) throws Exception {
        Unsafe unsafe = UnsafeUtil.getUnsafe();
        UnsafeSample unsafeSample = new UnsafeSample();

        Field field = UnsafeSample.class.getDeclaredField("normalInt");
        long objectFieldOffset = unsafe.objectFieldOffset(field);
        System.out.println(unsafe.getInt(unsafeSample, objectFieldOffset));

        Field sField = UnsafeSample.class.getDeclaredField("staticInt");
        Object base = unsafe.staticFieldBase(sField);
        long staticFieldOffset = unsafe.staticFieldOffset(sField);
        System.out.println(unsafe.getInt(base, staticFieldOffset));

    }
}