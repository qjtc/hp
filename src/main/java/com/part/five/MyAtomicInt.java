package com.part.five;

import com.part.UnsafeUtil;
import sun.misc.Unsafe;

import java.lang.reflect.Field;
import java.util.stream.IntStream;

public class MyAtomicInt {
    private volatile int value = 0;
    private static Unsafe unsafe;

    {
        unsafe = UnsafeUtil.getUnsafe();
    }

    public MyAtomicInt(int value) {
        this.value = value;
    }

    public MyAtomicInt() {
    }

    public int incrementAndGet(){
        int oldInt;
        Field field = null;
        try {
            field = MyAtomicInt.class.getDeclaredField("value");
            field.setAccessible(true);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        long o = unsafe.objectFieldOffset(field);
        do {
            oldInt = unsafe.getIntVolatile(this,o);
        } while(!unsafe.compareAndSwapInt(this, o, oldInt, oldInt+1));
        return oldInt;
    }

    public static void MyAtomicIntMain(){
        MyAtomicInt myAtomicInt = new MyAtomicInt(1);
        IntStream.range(1,100).forEach(i->new Thread(()-> System.out.println(myAtomicInt.incrementAndGet())).start());
    }
}