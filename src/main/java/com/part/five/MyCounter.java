package com.part.five;

import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.LongAdder;

/**
 * Created by Administrator on 2018/2/3.
 */
public class MyCounter {
    private long normalValue = 0L;
    private volatile long volatileValue = 0L;
    private AtomicLong atomicLong = new AtomicLong(0L);
    private LongAdder longAdder = new LongAdder();

    public synchronized void normalIncr(){
        normalValue++;
    }

    public synchronized void volatileIncr(){
        volatileValue++;
    }

    public void atomicIncr(){
        atomicLong.incrementAndGet();
    }

    public void longAdderIncr(){
        longAdder.increment();
    }

    public long getNormalValue() {
        return normalValue;
    }

    public long getVolatileValue() {
        return volatileValue;
    }

    public long getAtomicLong() {
        return atomicLong.get();
    }

    public long getLongAdder() {
        return longAdder.longValue();
    }
}
