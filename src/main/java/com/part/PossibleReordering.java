package com.part;

import java.util.stream.IntStream;

/**
 * Created by Administrator on 2018/1/29.
 */
public class PossibleReordering {
    int x = 0, y = 0;
    int a = 0, b = 0;
    public static void doTest(int i)
    {
        PossibleReordering ordering=new PossibleReordering();
        Thread one = new Thread(()->{ordering.a = 1;ordering.x =
                ordering.b;});
        Thread other = new Thread(()->{ordering.b = 1;ordering.y =
                ordering.a;});
        one.start();other.start();
        try {
            one.join();
            other.join();
        } catch (InterruptedException e) {
        }
        System.out.println(" run case :"+i+"(" + ordering.x + "," + ordering.y + ")");
    }
    public static void main(String[] args) throws InterruptedException {
        IntStream.range(0, 100).forEach(PossibleReordering::doTest);
    }
}
