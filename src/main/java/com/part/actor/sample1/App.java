package com.part.actor.sample1;

import akka.actor.*;
import akka.japi.pf.ReceiveBuilder;
import scala.PartialFunction;
import scala.runtime.BoxedUnit;

import java.util.stream.IntStream;

public class App {
    static class Counter extends AbstractLoggingActor {

        public static Props props() {
            return Props.create(Counter.class);
        }

        static class Message {
        }

        private int counter = 0;

        {
            receive(ReceiveBuilder
                    .match(Message.class, this::onMessage)
                    .build()
            );
        }

        private void onMessage(Message message) {
            counter++;
            log().info("Increased counter " + counter);
        }
    }

    static class Alarm extends AbstractLoggingActor {
        private final String password;

        private final PartialFunction<Object, BoxedUnit> enabled;
        private final PartialFunction<Object, BoxedUnit> disabled;

        public Alarm(String password) {
            this.password = password;

            enabled = ReceiveBuilder
                    .match(Activity.class, this::onActivity)
                    .match(Disable.class, this::onDisable)
                    .build();

            disabled = ReceiveBuilder
                    .match(Enable.class, this::onEnable)
                    .build();

            receive(disabled);
        }

        static class Activity {
        }

        static class Disable {
            private final String password;

            public Disable(String password) {
                this.password = password;
            }
        }

        static class Enable {
            private final String password;

            public Enable(String password) {
                this.password = password;
            }
        }

        public static Props props(String password) {
            return Props.create(Alarm.class, password);
        }

        private void onActivity(Activity ignored) {
            log().warning("oeoeoeoeoe, alarm alarm!!!");
        }

        private void onEnable(Enable enable) {
            if (password.equals(enable.password)) {
                log().info("Alarm enable");
                getContext().become(enabled);
            } else {
                log().info("Someone failed to enable the alarm");
            }
        }

        private void onDisable(Disable disable) {
            if (password.equals(disable.password)) {
                log().info("Alarm disabled");
                getContext().become(disabled);
            } else {
                log().warning("Someone who didn't know the password tried to disable it");
            }
        }
    }

    public static void alarmMain() {
        ActorSystem system = ActorSystem.create();
        final ActorRef alarm = system.actorOf(Alarm.props("cat"), "alarm");
        alarm.tell(new Alarm.Activity(), ActorRef.noSender());
        alarm.tell(new Alarm.Enable("dogs"), ActorRef.noSender());
        alarm.tell(new Alarm.Enable("cat"), ActorRef.noSender());
        alarm.tell(new Alarm.Activity(), ActorRef.noSender());
        alarm.tell(new Alarm.Disable("dogs"), ActorRef.noSender());
        alarm.tell(new Alarm.Disable("cat"), ActorRef.noSender());
        alarm.tell(new Alarm.Activity(), ActorRef.noSender());
    }

    public static void countMain() {
        ActorSystem system = ActorSystem.create("sample1");
        ActorRef counter = system.actorOf(Counter.props(), "counter");
        IntStream.range(1, 100).forEach((i) -> new Thread(() -> counter.tell(new Counter.Message(), ActorRef.noSender())).start());
    }
}
