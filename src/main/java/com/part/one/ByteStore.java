package com.part.one;

import java.util.concurrent.atomic.AtomicInteger;

public class ByteStore {
    private byte[] storeByteArry;
    private AtomicInteger i = new AtomicInteger(0);

    public ByteStore() {
        storeByteArry = new byte[3000];
    }

    public int size(){
        return i.intValue();
    }

    public void putMyItem(int index,MyItem item){
        if(index>size()||index>1000||index<0){
            throw new IndexOutOfBoundsException( "Index: "+index+", Size: "+size());
        }
        i.incrementAndGet();
        if(getMyItem(index).equals(item)){
            return;
        }
        storeByteArry[index*3]=item.getType();
        storeByteArry[index*3+1]=item.getColor();
        storeByteArry[index*3+2]=item.getPrice();
    }

    public MyItem getMyItem(int index){
        MyItem myItem = new MyItem();
        myItem.setType(storeByteArry[index*3]);
        myItem.setColor(storeByteArry[index*3+1]);
        myItem.setPrice(storeByteArry[index*3+2]);
        return myItem;
    }

    public byte[] getStoreByteArry() {
        return storeByteArry;
    }

//    public void setStoreByteArry(byte[] storeByteArry) {
//        this.storeByteArry = storeByteArry;
//    }
}
