package com.part.one;

public class SalaryGroup {
    private String groupMame;
    private int yearlySalarySum;
    private int count;

    public SalaryGroup() {
    }

    public SalaryGroup(String groupMame, int yearlySalarySum, int count) {
        this.groupMame = groupMame;
        this.yearlySalarySum = yearlySalarySum;
        this.count = count;
    }

    public String getGroupMame() {
        return groupMame;
    }

    public void setGroupMame(String groupMame) {
        this.groupMame = groupMame;
    }

    public int getYearlySalarySum() {
        return yearlySalarySum;
    }

    public void setYearlySalarySum(int yearlySalarySum) {
        this.yearlySalarySum = yearlySalarySum;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "SalaryGroup{" +
                "groupMame='" + groupMame + '\'' +
                ", yearlySalarySum=" + yearlySalarySum +
                ", count=" + count +
                '}';
    }
}
