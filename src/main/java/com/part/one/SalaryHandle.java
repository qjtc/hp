package com.part.one;

import org.apache.commons.collections4.CollectionUtils;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class SalaryHandle {
    static ThreadLocalRandom threadLocalRandom = ThreadLocalRandom.current();

    public Salary[] randomSalarys(int size){
        Salary[] salaries = new Salary[size];
        Stream.iterate(0,i -> ++i).limit(size).forEach(i-> salaries[i] = randomSalary(threadLocalRandom));
        return salaries;
    }

    /**
     *
     * @param path 文件路径
     * @param size 记录条数
     * @throws IOException
     */
    public void randomSalarysIntoFile(String path,int size) throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream(new File(path));
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream,"utf-8");
        BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);
        IntStream.range(0,size).parallel().forEach(i->{
            Salary salary = randomSalary(threadLocalRandom);
            String s = salary.getName() + "," + salary.getBaseSalary() + "," +salary.getBonus();
            try {
                bufferedWriter.write(s + "\n");
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        bufferedWriter.close();
        outputStreamWriter.close();
        fileOutputStream.close();
    }

    public void randomSalarysIntoFile2(String path,int size) throws IOException {
        ByteBuffer buf = ByteBuffer.allocate(1024);
        RandomAccessFile aFile = new RandomAccessFile(path, "rw");
        FileChannel inChannel = aFile.getChannel();
        IntStream.range(0,size).forEach(i->{
            Salary salary = randomSalary(threadLocalRandom);
            String s = salary.getName() + "," + salary.getBaseSalary() + "," +salary.getBonus()+"\n";
            try {
                buf.clear();
                buf.put(s.getBytes());
                buf.flip();
                while (buf.hasRemaining())
                    inChannel.write(buf);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public void sortSalaryGroup(String path,int size) throws IOException {
        FileReader in = new FileReader(new File(path));
        LineNumberReader reader = new LineNumberReader(in);
        Map<String,IntSummaryStatistics> map = IntStream.range(1,size).mapToObj(n-> {
            String line = null;
            try {
                line = reader.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return line;
        }).collect(Collectors.groupingBy((String s)->s.substring(0,2),
                Collectors.mapping((String s)->s.substring(s.indexOf(",")+1),Collectors.summarizingInt((String s)-> {
                    String baseSalary  = s.substring(0,s.indexOf(",")) ;
                    String bonus  = s.substring(s.indexOf(",")+1) ;
                    return getYearlySalary(Integer.parseInt(baseSalary),Integer.parseInt(bonus));
                }))));
        List<SalaryGroup> salaryGroups = new ArrayList<>();
        map.forEach((k,v)->{
            SalaryGroup salaryGroup = new SalaryGroup(k, (int)v.getSum(), (int)v.getCount());
            salaryGroups.add(salaryGroup);
        });
        salaryGroups.stream().sorted(Comparator.comparing((SalaryGroup salaryGroup) -> salaryGroup.getYearlySalarySum()).reversed()).limit(10)
                .forEach((SalaryGroup salaryGroup)-> System.out.println(salaryGroup.getGroupMame()+","+salaryGroup.getYearlySalarySum()+","+salaryGroup.getCount()+"人"));
    }

    public Salary randomSalary(ThreadLocalRandom threadLocalRandom){
        return new Salary(randomName(5,threadLocalRandom),threadLocalRandom.nextInt(999995)+5,threadLocalRandom.nextInt(100000));
    }

    public String randomName(int length,ThreadLocalRandom threadLocalRandom){
        String KeyString = "abcdefghijklmnopqrstuvwxyz";
        StringBuffer sb = new StringBuffer();
        IntStream.range(1,length).forEach(i-> sb.append(KeyString.charAt(threadLocalRandom.nextInt(KeyString.length()))));
        return sb.toString();
    }

    public Salary[] salarySort(Salary[] salaries){
        Collections.sort(Arrays.asList(salaries),Comparator.comparing((Salary s)->s.getBaseSalary() * 13 + s.getBonus()).reversed());
        return salaries;
    }

    public Salary[] salaryFilterSort(Salary[] salaries){
        List<Salary> salaryList = Arrays.asList(salaries);
        return salaryList.stream().filter((Salary salary)->getYearlySalary(salary) > 100000).sorted(Comparator.comparing((Salary salary)->getYearlySalary(salary)).reversed()).collect(Collectors.toList()).toArray(salaries);
    }

    public Salary[] salaryParallelFilterSort(Salary[] salaries){
        List<Salary> salaryList = Arrays.asList(salaries);
        return salaryList.parallelStream().filter((Salary salary)->getYearlySalary(salary) > 100000).sorted(Comparator.comparing((Salary salary)->getYearlySalary(salary)).reversed()).collect(Collectors.toList()).toArray(salaries);
    }
    /**
     * 计算年薪
     * @param salary
     * @return
     */
    public int getYearlySalary(Salary salary){
        return salary.getBaseSalary() * 13 + salary.getBonus();
    }

    public static int getYearlySalary(int baseSalary,int bonus){
        return baseSalary * 13 + bonus;
    }

    public void quickSort(Salary[] a, int low, int high){
        if(low < high){
            int privotLoc = partition(a,  low,  high);  //将表一分为二
            quickSort(a, low, privotLoc -1);          //递归对低子表递归排序
            quickSort(a, privotLoc + 1, high);        //递归对高子表递归排序
        }
    }

    private String salaryString(Salary salary){
        return salary.getName()+":"+(salary.getBaseSalary()*13+salary.getBonus());
    }

    public void printSalay(Salary[] s){
        Collections.reverse(Arrays.asList(s));
        System.out.println(CollectionUtils.collect(Arrays.asList(s),salary->salaryString(salary)).toString());
    }

    private int partition(Salary[] a, int low, int high) {
        int privotKey = a[low].getBaseSalary() * 13 + a[low].getBonus();
        while(low < high){
            while(low < high  && (a[high].getBaseSalary() * 13 + a[high].getBonus()) >= privotKey){
                --high;  //从high 所指位置向前搜索，至多到low+1 位置。将比基准元素小的交换到低端
            }
            swap(low,high,a);
            while(low < high  && (a[low].getBaseSalary() * 13 + a[low].getBonus()) <= privotKey ){
                ++low;
            }
            swap(low,high,a);
        }
        return low;
    }

    private void swap(int low, int high,Salary[] a) {
        Salary s = a[low];
        a[low] = a[high];
        a[high] = s;
    }

    public byte[] intToBytes (int i){
        byte[] b=new byte[4];
//        0xff的二进制是第四位为8个1 其他是0的数，作用就是排除不想要的位
        b[0]=(byte) (i>>24);
        b[1]=(byte) ((i>>16)&0xff);
        b[2]=(byte) ((i>>8)&0xff);
        b[3]=(byte) (i&0xff);
        return b;
    }

    public int bytesToInt (byte[] b){
        int i = 0;
        i+=((b[0]&0xff)<<24);
        i+=((b[1]&0xff)<<16);
        i+=((b[2]&0xff)<<8);
        i+=((b[3]&0xff));
        return i;
    }
}
