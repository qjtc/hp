package com.part.one;

public class ByteStoreHandle {
    //冒泡排序
    public byte[] bubbleSort(ByteStore byteStore){
        byte[] storeByteArry = byteStore.getStoreByteArry();
        int length = byteStore.size()-1;
        for(int i=0;i<length;i++){//外层循环控制排序趟数
            for(int j=0;j<length-i;j++){//内层循环控制每一趟排序多少次
                if(storeByteArry[j*3+2]<storeByteArry[(j+1)*3+2]){
                    int index = j*3;
                    int bigIndex = (j+1)*3;
                    byte type = storeByteArry[index];
                    storeByteArry[index]=storeByteArry[bigIndex];
                    storeByteArry[bigIndex]=type;

                    index++;
                    bigIndex++;
                    byte color = storeByteArry[index];
                    storeByteArry[index]=storeByteArry[bigIndex];
                    storeByteArry[bigIndex]=color;

                    index++;
                    bigIndex++;
                    byte price = storeByteArry[index];
                    storeByteArry[index]=storeByteArry[bigIndex];
                    storeByteArry[bigIndex]=price;
                }
            }
        }
        return storeByteArry;
    }
}
