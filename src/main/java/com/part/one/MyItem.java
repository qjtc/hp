package com.part.one;

public class MyItem {
    private byte type;
    private byte color;
    private byte price;

    public byte getType() {
        return type;
    }

    public void setType(byte type) {
        this.type = type;
    }

    public byte getColor() {
        return color;
    }

    public void setColor(byte color) {
        this.color = color;
    }

    public byte getPrice() {
        return price;
    }

    public void setPrice(byte price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MyItem myItem = (MyItem) o;

        if (type != myItem.type) return false;
        if (color != myItem.color) return false;
        return price == myItem.price;
    }

    @Override
    public int hashCode() {
        int result = (int) type;
        result = 31 * result + (int) color;
        result = 31 * result + (int) price;
        return result;
    }

    @Override
    public String toString() {
        return "MyItem{" +
                "type=" + type +
                ", color=" + color +
                ", price=" + price +
                '}';
    }
}
