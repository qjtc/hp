import com.part.one.Salary;
import com.part.one.SalaryHandle;
import org.apache.commons.lang3.time.StopWatch;
import org.junit.Test;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

public class Test4 {

    /**
     * 1 说明Stream 与Collection的区别 以及关系
     *
     * @throws Exception
     */
    @Test
    public void q1() throws Exception {
//        集合是一种内存中的数据结构，主要用于存储数据,可以进行新增，删除等操作
//        流不能进行新增，删除,其中元素是根据需求计算获得,需求驱动
//        循环方式也有区别，流没有迭代器而且消费完就没有了
    }

    /**
     * 2 下面代码为什么输出流中的每个元素2遍
     *
     * @throws Exception
     */
    @Test
    public void q2() throws Exception {
//        Intermediate：map (mapToInt, flatMap 等)、 filter、 distinct、 sorted、 peek、 skip、 parallel、 sequential、 unordered
//        Terminal：forEach、 forEachOrdered、 toArray、 reduce、 collect、 min、 max、 count、iterator
//        Short-circuiting：
//        anyMatch、 allMatch、 noneMatch、 findFirst、 findAny、 limit
//        流的使用一般包括三件事：
//        一个数据源（如集合）来执行一个查询
//        一个中间操作链，形成一条流的流水线
//        一个终端操作，执行流水线，并能生成结果
//        其中中间操作链可以进行循环合并，在终端操作时候一次性处理
        Stream.of("d2", "a2", "b1", "b3", "c").filter(s -> {
            System.out.println("filter: " + s);
            return true;
        }).forEach(s -> System.out.println("forEach: " + s));
    }

//3  用Stream的API实现第四题的结果，其中增加一个过滤条件，即年薪大于10万的才被累加，分别用ParellStream与普通Stream来运算，看看效果的差距
    @Test
    public void q3() throws Exception {
        SalaryHandle salaryHandle = new SalaryHandle();
        Salary[] salaries = salaryHandle.randomSalarys(100000);
        StopWatch stopWatch = StopWatch.createStarted();
        salaries = salaryHandle.salaryFilterSort(salaries);
        System.out.println(stopWatch.getTime(TimeUnit.MICROSECONDS));//260496
//        salaries = salaryHandle.salaryParallelFilterSort(salaries);
//        System.out.println(stopWatch.getTime(TimeUnit.MICROSECONDS));//216835
        System.out.println(Arrays.toString(Arrays.copyOf(salaries,10)));
    }

//4 自己动手编写不少于5个Stream的例子，并解释代码
//
//    加分题：
//            1  用自定义的Collect实现第三题的功能
    @Test
    public void q4() throws Exception {

    }
}
