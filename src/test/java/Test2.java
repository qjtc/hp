import com.part.one.SalaryHandle;
import com.part.two.IntAndByte;
import com.part.two.WordsMain;
import org.apache.commons.lang3.time.StopWatch;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

public class Test2 {

    /**
     * 1 得到 String s="中国" 这个字符串的utf-8编码，gbk编码，iso-8859-1编码的字符串，看看各自有多少字节，同时解释为什么以utf-8编码得到的byte[]无法用gbk的方式“还原”为原来的字符串
     *
     * @throws Exception
     */
    @Test
    public void q1() throws Exception {
        //        1UTF-8的编码规则很简单，只有二条：
        //        1）对于单字节的符号，字节的第一位设为0，后面7位为这个符号的unicode码。因此对于英语字母，UTF-8编码和ASCII码是相同的；
        //        2）对于n字节的符号，第一个字节的前n位都设为1，第n+1位设为0，后面字节的前两位一律设为10。剩下的二进制位，表示为这个符号的unicode码；
        //        GBK采用双字节表示，即不论中、英文字符均使用双字节来表示，为了区分中文，将其最高位都设定成1，总体编码范围为8140-FEFE之间，首字节在81-FE 之间，尾字节在40-FE 之间，剔除 XX7F 一条线。
        String s = "中国";
        byte[] utf8Bytes = s.getBytes("utf-8");
        byte[] gbkBytes = s.getBytes("gbk");
        byte[] iso88591Bytes = s.getBytes("iso-8859-1");
        System.out.println(" utf8Bytes:"+utf8Bytes.length+" gbkBytes:"+gbkBytes.length+" iso88591Bytes:"+iso88591Bytes.length);
    }

    /**
     * 2 分别用大头和小头模式将整数 a=10240写入到文件中（4个字节），并且再正确读出来，打印到屏幕上，同时截图UltraEdit里的二进制字节序列，做对比说明
     *
     * @throws Exception
     */
    @Test
    public void q2() throws Exception {
        IntAndByte intAndByte = new IntAndByte();
        int i = 10240;
        byte[] bytes = intAndByte.intToByteForBig(i);
        int k = intAndByte.byteToIntForBig(bytes);
        System.out.println(k);
        bytes = intAndByte.intToByteForLittle(i);
        intAndByte.output(bytes);
        k = intAndByte.byteToIntForLittle(bytes);
        System.out.println(k);
        intAndByte.writeFile("D:\\output.txt",null);
    }

    /**
     * 3 整理全套的Java IO类图并用PPT讲解说明
     *
     * @throws Exception
     */
    @Test
    public void q3() throws Exception {
        //        选择java.io包，idea直接导出对应的类图
    }

    /**
     * 4  随机生成 Salary {name, baseSalary, bonus  }的记录，如“wxxx,10,1”，每行一条记录，总共1000万记录，写入文本文件（UFT-8编码），
     * 然后读取文件，name的前两个字符相同的，其年薪累加，比如wx，100万，3个人，最后做排序和分组，输出年薪总额最高的10组：
     *  wx, 200万，10人
     *  lt, 180万，8人
     *
     * @throws Exception
     */
    @Test
    public void q4() throws Exception {
        SalaryHandle salaryHandle = new SalaryHandle();
        String path = "D:\\salary\\s2.txt";
        int size = 10000000;
        StopWatch stopWatch = StopWatch.createStarted();
        salaryHandle.randomSalarysIntoFile(path,size);
        System.out.println(stopWatch.getTime(TimeUnit.MICROSECONDS));
//        salaryHandle.sortSalaryGroupOld(path,size);
        salaryHandle.sortSalaryGroup(path,size);
        System.out.println(stopWatch.getTime(TimeUnit.MICROSECONDS));
//        ct,1128596,15270
//        ku,1127128,15187
//        cl,1124940,15076
//        pa,1124113,15066
//        zi,1123613,15071
//        yo,1123317,15143
//        mj,1123223,14993
//        ck,1122492,15056
//        ws,1121681,15137
//        nh,1119256,15078
//        330
    }

    /**
     * 加分题
     * 1：用装饰者模式实现如下的功能：
     * 要求用户输入一段文字，比如 Hello Me，然后屏幕输出几个选项
     *     1 ：加密
     *     2 ：反转字符串
     *     3：转成大写
     *     4：转成小写
     *     5：扩展或者剪裁到10个字符，不足部分用！补充
     *     6:用户输入 任意组合，比如 1，3 表示先执行1的逻辑，再执行3的逻辑
     *     根据用户输入的选择，进行处理后，输出结果
     *
     * @throws Exception
     */
    @Test
    public void jq1() throws Exception {
        WordsMain wordsMain = new WordsMain();
        wordsMain.printMain();
    }

    /**
     * 2: 用FileChannel的方式实现第四题，注意编码转换问题，并对比性能
     * @throws Exception
     */
    @Test
    public void jq2() throws Exception {
        StopWatch stopWatch = StopWatch.createStarted();
        String path = "D:\\salary\\s2.txt";
        int size = 10000000;
        SalaryHandle salaryHandle = new SalaryHandle();
        salaryHandle.randomSalarysIntoFile2(path,size);
        System.out.println(stopWatch.getTime(TimeUnit.MICROSECONDS));

    }
}
