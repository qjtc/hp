import org.junit.Test;

import java.util.Arrays;
import java.util.IdentityHashMap;

public class Test3 {

    //1 分析Collection接口以及其子接口，很通俗的方式说说，究竟有哪些类型的Collection，各自解决什么样的问题
// List, 按进入先后有序保存,可重复
// Queue,队列模式
// Set,不允许有相同元素
//2 TreeSet继承了什么Set，与HashSet的区别是？HashSet与HashTable是“一脉相承”的么？
//public class TreeSet<E> extends AbstractSet<E>
//        implements NavigableSet<E>, Cloneable, java.io.Serializable
//public class HashSet<E>
//        extends AbstractSet<E>
//        implements Set<E>, Cloneable, java.io.Serializable
//public class Hashtable<K,V>
//        extends Dictionary<K,V>
//        implements Map<K,V>, Cloneable, java.io.Serializable
//    HashSet是通过HashMap实现,TreeSet是通过TreeMap实现,名字上差距就是在hash，tree上,hash就是hash表，离散分部，tree二叉树，可以具有排序属性
//    set,明显特征就是数据不能重复，其中HashSet通过标准的hashcode，eques进行比较值，TreeSet通过equals或者CompareTo
//    HashSet与HashTable从继承关系上看明显不是一家人
//            3 Queue接口增加了哪些方法，这些方法的作用和区别是？
//    boolean add(E e); 继承Collection 添加元素到队列,和offer一样
//public boolean offerLast(E e) {
//    addLast(e);
//    return true;
//}
//    boolean offer(E e); 添加元素到队列
//    E remove(); 继承Collection和poll方法实现一样，null做了校验
//public E removeFirst() {
//    E x = pollFirst();
//    if (x == null)
//        throw new NoSuchElementException();
//    return x;
//}
//    E poll(); 返回第一个元素，并在队列中删除
//    E element(); 返回第一个元素，不删除数据，和peek差不多，多了null做了校验
//public E getFirst() {
//    @SuppressWarnings("unchecked")
//    E result = (E) elements[head];
//    if (result == null)
//        throw new NoSuchElementException();
//    return result;
//}
//    E peek();返回第一个元素，不删除数据
//            4 LinkedList也是一种Queue么？是否是双向链表?
//    public class LinkedList<E>
//            extends AbstractSequentialList<E>
//            implements List<E>, Deque<E>, Cloneable, java.io.Serializable
//    {
//        transient int size = 0;
//        transient java.util.LinkedList.Node<E> first;
//        transient java.util.LinkedList.Node<E> last;
//    LinkedList是双向链表，从继承关系上看，还是有血缘关系的，所以LinkedList是一种Queue
//
    /**
     * 5 Java数组如何与Collection相互转换
     *
     * @throws Exception
     */
    @Test
    public void q5() throws Exception {
        String[] strings = {"s","t","r","i","n","g"};
        Arrays.asList(strings).toArray();
    }
    //6 Map的一级子接口有哪些种类，分别用作什么目的？
//    SortedMap：它用来保持键的有序顺序。
//    ConcurrentMap：线程安全
//7 HashSet 与HashMap中放入的自定义对象必须要实现哪些方法，说明原因
//    hashcode，equals方法,添加数据时候需要先通过判断hashcode，当hash冲突时候在equals判断对象是否相等
//8 TreeSet里的自定义对象必须要实现什么方法，说明原因
//    public TreeSet(Comparator<? super E> comparator) {
//        this(new TreeMap<>(comparator));
//    }
//    TreeSet是有序的，所以需要实现自定义的比较器
//9 LinkedHashMap使用什么来保存数据，其效率与HashMap相比如何？它又有什么独特特性
//public class LinkedHashMap<K,V>
//        extends HashMap<K,V>
//        implements Map<K,V>
//    static class Entry<K,V> extends HashMap.Node<K,V> {
//        LinkedHashMap.Entry<K,V> before, after;
//        Entry(int hash, K key, V value, HashMap.Node<K,V> next) {
//            super(hash, key, value, next);
//        }
//    }
//    LinkedHashMap，双向循环链表，维护顺序，其他和hashmap相似。
//10 IdentityHashMap 里面如果按照下面的方法放入对象，分别是什么结果，请解释原因
//        private boolean containsMapping(Object key, Object value) {
//            Object k = maskNull(key);
//            Object[] tab = table;
//            int len = tab.length;
//            int i = hash(k, len);
//            while (true) {
//                Object item = tab[i];
//                if (item == k)
//                    return tab[i + 1] == value;
//                if (item == null)
//                    return false;
//                i = nextKeyIndex(i, len);
//            }
//        }
//    private static class IntegerCache {
//        static final int low = -128;
//        static final int high;
//        static final Integer cache[];
//
//        static {
//            // high value may be configured by property
//            int h = 127;
//            String integerCacheHighPropValue =
//                    sun.misc.VM.getSavedProperty("java.lang.Integer.IntegerCache.high");
//            if (integerCacheHighPropValue != null) {
//                try {
//                    int i = parseInt(integerCacheHighPropValue);
//                    i = Math.max(i, 127);
//                    // Maximum array size is Integer.MAX_VALUE
//                    h = Math.min(i, Integer.MAX_VALUE - (-low) -1);
//                } catch( NumberFormatException nfe) {
//                    // If the property cannot be parsed into an int, ignore it.
//                }
//            }
//            high = h;
//
//            cache = new Integer[(high - low) + 1];
//            int j = low;
//            for(int k = 0; k < cache.length; k++)
//                cache[k] = new Integer(j++);
//
//            // range [-128, 127] must be interned (JLS7 5.1.7)
//            assert Integer.IntegerCache.high >= 127;
//        }
//
//        private IntegerCache() {}
//    }
    @Test
    public void q6() throws Exception {
        IdentityHashMap map = new IdentityHashMap();
        Integer a=5;
        Integer b=5;
        map.put(a,"100");
        map.put(b,"100");
        System.out.println(map.size());
        map.clear();
        a=Integer.MAX_VALUE-1;
        b=Integer.MAX_VALUE-1;
        map.put(a,"100");
        map.put(b,"100");
        System.out.println(map.size());
        map.clear();
        a=127;
        b=127;
        map.put(a,"100");
        map.put(b,"100");
        System.out.println(map.size());
        map.clear();
        a=128;
        b=128;
        map.put(a,"100");
        map.put(b,"100");
        System.out.println(map.size());
//        Integer方法有缓存策略，IdentityHashMap通过==判断相等，而不是equals方法，最后两个明显是俩个对象。
    }
//    加分题，
//    给出ＪＤＫ　１.８的java 集合框架全图谱（Class类图）， 并标明1.7与1.8里出现的新的类，解释其目的
}
