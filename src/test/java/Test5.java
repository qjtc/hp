import com.part.actor.sample1.App;
import com.part.five.MyAtomicInt;
import com.part.five.MyCounterMain;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

public class Test5 {

    /**
     * 1 解释下你所理解的Happens-before含义和JVM里的几个Happens-before约定
     *
     *
     * @throws Exception
     */
    @Test
    public void q1() throws Exception {
//        java并发编程实战
//        程序顺序规则：如果程序中操作A在操作B之前，那么在线程中A操作将在B操作之前执行。
//        监视器锁规则：在监视器锁上的解锁操作必须在同一个监视器锁上的加锁操作之前执行。
//        volatile变量规则：对vlolatile变量的写操作必须在对该变量的读操作之前执行。
//        线程启动规则：在线程上对Thread.start的调用必须在该线程中执行任何操作之前执行。
//        线程结束规则：线程中的任何操作都必须在其他线程检测到该线程已经结束之前执行，或者从Thread.join中成功返回，或者在调用Thread.isAlive时返回false。
//        中断规则：当一个线程在另一个线程上调用interrupt时，必须在被中断线程检测到interrupt调用之前执行（通过抛出InterruptedException，或者调用isInterrupted和interrupted）。
//        终接器规则：对象的构造函数必须在启动该对象的终接器之前执行。
//        传递性：如果操作A在操作B之前执行，并且操作B在操作C之前执行，那么操作A必须在操作C之前执行。

//        jvm虚拟机实战
//        程序次序规则（Program Order Rule）：在一个线程内，按照程序代码顺序，书写在前面
//        的操作先行发生于书写在后面的操作。准确地说，应该是控制流顺序而不是程序代码顺序，
//        因为要考虑分支、循环等结构。
//        管程锁定规则（Monitor Lock Rule）：一个unlock操作先行发生于后面对同一个锁的lock
//        操作。这里必须强调的是同一个锁，而“后面”是指时间上的先后顺序。
//        volatile变量规则（Volatile Variable Rule）：对一个volatile变量的写操作先行发生于后面
//        对这个变量的读操作，这里的“后面”同样是指时间上的先后顺序。
//        线程启动规则（Thread Start Rule）：Thread对象的start（）方法先行发生于此线程的每
//        一个动作。
//        线程终止规则（Thread Termination Rule）：线程中的所有操作都先行发生于对此线程的
//        终止检测，我们可以通过Thread.join（）方法结束、Thread.isAlive（）的返回值等手段检测
//        到线程已经终止执行。
//        线程中断规则（Thread Interruption Rule）：对线程interrupt（）方法的调用先行发生于被
//        中断线程的代码检测到中断事件的发生，可以通过Thread.interrupted（）方法检测到是否有
//        中断发生。
//        对象终结规则（Finalizer Rule）：一个对象的初始化完成（构造函数执行结束）先行发
//        生于它的finalize（）方法的开始。
//        传递性（Transitivity）：如果操作A先行发生于操作B，操作B先行发生于操作C，那就
//        可以得出操作A先行发生于操作C的结论。
    }

    /**
     * 2 不依赖任何的同步机制（syncronized ,lock），有几种方式能实现多个线程共享变量之间的happens-before方式
     *
     * @throws Exception
     */
    @Test
    public void q2() throws Exception {
//        actor
//        unsafe,volatile
        App.alarmMain();
//        App.countMain();
//        MyAtomicInt.MyAtomicIntMain();
        TimeUnit.SECONDS.sleep(3);
    }

    /**
     * 3 编程验证normal var ,volaitle，synchronize,atomicLong ,LongAdder，这几种做法实现的计数器方法，在多线程情况下的性能，准确度
     * class MyCounter
     * {
     * private long value;//根据需要进行替换
     * public void incr();
     * public long getCurValue();//得到最后结果
     * }
     * 启动10个线程一起执行，每个线程调用incr() 100万次，
     * 所有线程结束后，打印 getCurValue()的结果，分析程序的结果 并作出解释。 用Stream和函数式编程实现则加分！
     *
     * @throws Exception
     */
    @Test
    public void q3() throws Exception {
//        count:10000000 longAdderTest:177204
//        count:10000000 atomicTest:249041
//        count:10000000 normalTest:377015
//        count:10000000 volatileTest:398923

        MyCounterMain.longAdderTest();
        MyCounterMain.atomicTest();
        MyCounterMain.normalTest();
        MyCounterMain.volatileTest();

//<>代表效率,主要根据以上叠加计算得出以下结论。
//volatile + synchronized < atomic = volatile + cas,
//normal + synchronized > volatile + synchronized = normal + fence + synchronized
//LongAdder = cas + volatile + longAccumulate > AtomicLong = cas + volatile
    }

    /**
     * 1 自己画出Java内存模型并解释各个区，以JDK8为例，每个区的控制参数也给出。
     *
     * @throws Exception
     */
    @Test
    public void xq1() throws Exception {
        // 1、虚拟机栈：每个线程有一个私有的栈，随着线程的创建而创建。
        // 栈里面存着的是一种叫“栈帧”的东西，每个方法会创建一个栈帧，
        // 栈帧中存放了局部变量表（基本数据类型和对象引用）、操作数栈、方法出口等信息。
        // 栈的大小可以固定也可以动态扩展。
        // 当栈调用深度大于JVM所允许的范围，会抛出StackOverflowError的错误，不过这个深度范围不是一个恒定的值，我们通过下面这段程序可以测试一下这个结果：
//        StackErrorMock.stackErrorMockMain();
        // 2、本地方法栈：这部分主要与虚拟机用到的 Native 方法相关，一般情况下， Java 应用程序员并不需要关心这部分的内容。
        // 3、PC 寄存器：PC 寄存器，也叫程序计数器。JVM支持多个线程同时运行，每个线程都有自己的程序计数器。倘若当前执行的是 JVM 的方法，则该寄存器中保存当前执行指令的地址；倘若执行的是native 方法，则PC寄存器中为空。
        // 4、堆：堆内存是 JVM 所有线程共享的部分，在虚拟机启动的时候就已经创建。所有的对象和数组都在堆上进行分配。这部分空间可通过 GC 进行回收。当申请不到空间时会抛出 OutOfMemoryError。
        // 5、方法区：方法区也是所有线程共享。主要用于存储类的信息、常量池、方法数据、方法代码等。方法区逻辑上属于堆的一部分，但是为了与堆进行区分，通常又叫“非堆”。 关于方法区内存溢出的问题会在下文中详细探讨。
//        Xss：每个线程的stack大小（栈）
//        Xmx：JAVA HEAP的最大值、默认为物理内存的1/4
//        Xms：JAVA HEAP的初始值，server端最好Xms与Xmx一样
//        Xmn：JAVA HEAP young区的大小
//        XX:PermSize：设定内存的永久保存区域
//        XX:MaxPermSize：设定最大内存的永久保存区域
//        在JDK1.8中，取消了PermGen，取而代之的是Metaspace，所以PermSize和MaxPermSize参数失效，取而代之的是
//        -XX:MetaspaceSize=64m -XX:MaxMetaspaceSize=128m
    }

    /**
     * 2 解释为什么会有数组的Atomic类型的对象
     * @throws Exception
     */
    @Test
    public void xq2() throws Exception {
//        public final void set(int i, int newValue) {
//            unsafe.putIntVolatile(array, checkedByteOffset(i), newValue);
//        }
//        private int getRaw(long offset) {
//            return unsafe.getIntVolatile(array, offset);
//        }
//        public final int getAndAdd(int i, int delta) {
//            return unsafe.getAndAddInt(array, checkedByteOffset(i), delta);
//        }
//        AtomicIntegerArray和AtomicInteger类似，都是通过cas，和volatile保证数据可见性
    }
}
